﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            submit.Click += new EventHandler(submit_Click);  //Checking for submit click
        }

        private void submit_Click(Object sender, EventArgs e)
        {
            string input = earthWeight.Text;  //Grabbing input from text box
            double translatedInput = double.Parse(input);  //Translating string into an double

            double output = (translatedInput / 9.81) * 3.711;  //Calculation of formula in the weight of m/s^2
            string outputString = output.ToString();  //Translating double back into string

            marsWeight.Text = outputString;  //Outputting string to text box

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
