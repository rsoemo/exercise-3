﻿namespace Exercise_3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.marsWeight = new System.Windows.Forms.TextBox();
            this.submit = new System.Windows.Forms.Button();
            this.earthText = new System.Windows.Forms.Label();
            this.marsTag = new System.Windows.Forms.Label();
            this.earthWeight = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // marsWeight
            // 
            this.marsWeight.Location = new System.Drawing.Point(127, 85);
            this.marsWeight.Name = "marsWeight";
            this.marsWeight.Size = new System.Drawing.Size(100, 20);
            this.marsWeight.TabIndex = 1;
            // 
            // submit
            // 
            this.submit.Location = new System.Drawing.Point(127, 111);
            this.submit.Name = "submit";
            this.submit.Size = new System.Drawing.Size(100, 23);
            this.submit.TabIndex = 2;
            this.submit.Text = "Submit";
            this.submit.UseVisualStyleBackColor = true;
            // 
            // earthText
            // 
            this.earthText.AutoSize = true;
            this.earthText.Location = new System.Drawing.Point(52, 62);
            this.earthText.Name = "earthText";
            this.earthText.Size = new System.Drawing.Size(72, 13);
            this.earthText.TabIndex = 3;
            this.earthText.Text = "Earth Weight:";
            this.earthText.Click += new System.EventHandler(this.label1_Click);
            // 
            // marsTag
            // 
            this.marsTag.AutoSize = true;
            this.marsTag.Location = new System.Drawing.Point(54, 88);
            this.marsTag.Name = "marsTag";
            this.marsTag.Size = new System.Drawing.Size(70, 13);
            this.marsTag.TabIndex = 4;
            this.marsTag.Text = "Mars Weight:";
            // 
            // earthWeight
            // 
            this.earthWeight.Location = new System.Drawing.Point(127, 59);
            this.earthWeight.Name = "earthWeight";
            this.earthWeight.Size = new System.Drawing.Size(100, 20);
            this.earthWeight.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(-4, -5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(347, 217);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Firebrick;
            this.ClientSize = new System.Drawing.Size(343, 207);
            this.Controls.Add(this.marsTag);
            this.Controls.Add(this.earthText);
            this.Controls.Add(this.submit);
            this.Controls.Add(this.marsWeight);
            this.Controls.Add(this.earthWeight);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Your Weight on Mars";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox marsWeight;
        private System.Windows.Forms.Button submit;
        private System.Windows.Forms.Label earthText;
        private System.Windows.Forms.Label marsTag;
        private System.Windows.Forms.TextBox earthWeight;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

